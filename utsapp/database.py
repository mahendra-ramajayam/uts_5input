from datetime import datetime
from utsapp import db
# from flask_login import UserMixin


# db.session.add(me)
# db.session.commit()

# SQLAlchemy each class becomes a table

class Composition(db.Model):
    __tablename__ = 'composition'
    id = db.Column(db.Integer, primary_key=True)

    si = db.Column(db.Float, nullable=True)
    fe = db.Column(db.Float, nullable=True)
    mn = db.Column(db.Float, nullable=True)
    mg = db.Column(db.Float, nullable=True)
    cr = db.Column(db.Float, nullable=True)
    # predicted_value = db.Column(db.Float, nullable=True)
    # true_value = db.Column(db.Float, nullable=True)
    # date_comp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f"Composition('{self.id}')"

class Database(object):

    @staticmethod
    def initialize():
        db.create_all()
