from flask import render_template, url_for, flash, redirect, request
from utsapp import app, db
from utsapp.forms import CompositionForm
from utsapp.database import Composition, Database
import pandas as pd
from sklearn.externals import joblib
import os, pickle

@app.route("/")
@app.route("/home")
def home():
    # posts = Composition.query.all()
    form = CompositionForm()
    return render_template('home.html', form=form, output='') # posts=posts,


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/composition_input", methods=['GET', 'POST'])
def composition_input():
    form = CompositionForm()
    if request.method == 'POST':
        flash('Your data has been created!', 'success')
        if form.validate_on_submit():
            composition = Composition(si=form.si.data, fe=form.fe.data, mn=form.mn.data, mg=form.mg.data, cr=form.cr.data)
             # fe=form.fe.data,
            db.session.add(composition)
            db.session.commit()
            return redirect(url_for('home'))

    return render_template('composition_input.html', title='Input', form=form)

# @app.route('/')
# def index():
#     form = EntryForm(request.form)
#     return render_template('entry.html', form=form, z='')


@app.route('/results', methods=['GET', 'POST'])
def results():
    form = CompositionForm()
    z = 'Bad Input'
    if request.method == 'POST':
        flash('Your data has been created!', 'success')
        if form.validate_on_submit():
            si = request.form['si']
            fe = request.form['fe']
            mn = request.form['mn']
            mg = request.form['mg']
            cr = request.form['cr']
            raw_input = [si, fe, mn, mg, cr]
            destination = os.path.join('utsapp', 'pkl_models')
            loaded_xss = pickle.load(open(destination+'/x_ss.pkl', 'rb'))
            loaded_yss = pickle.load(open(destination+'/y_ss.pkl', 'rb'))
            loaded_model = pickle.load(open(destination+'/dummy_5input_classifier.pkl', 'rb'))

            transformed_input = loaded_xss.transform([raw_input])
            raw_prediction = loaded_model.predict(transformed_input)
            transformed_prediction = loaded_yss.inverse_transform([raw_prediction])

            prediction = loaded_model.predict([[si, fe, mn, mg, cr]])
            # return str(prediction)
            return render_template('home.html', form=form, output=str(transformed_prediction[0][0]))
    return render_template('home.html', form=form, output=z )
