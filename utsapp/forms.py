from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, IntegerField, DecimalField
from wtforms.validators import NumberRange, ValidationError
# from kerasapp.database import User, Composition


class CompositionForm(FlaskForm):
    si = DecimalField("Silicon", validators=[NumberRange(min=0.0000, max=90.000)],  places=5)
    fe = DecimalField("Iron", validators=[NumberRange(min=0.0000, max=90.000)],  places=5)
    mn = DecimalField("Manganese", validators=[NumberRange(min=0.0000, max=90.000)],  places=5)
    mg = DecimalField("Magnesium", validators=[NumberRange(min=0.0000, max=90.000)],  places=5)
    cr = DecimalField("Chromium", validators=[NumberRange(min=0.0000, max=90.000)],  places=5)
    # predicted_value = DecimalField("PredictedY", validators=[NumberRange(min=0.0000, max=10000.000)],  places=5)
    # true_value = DecimalField("TrueY", validators=[NumberRange(min=0.0000, max=1000.000)],  places=5)

    submit = SubmitField('Push')
